import Config

config :blog_engine,
  # path_to_posts: "/Users/andreas/Projects/Elixir/awsd/blog/content/posts/**/*.md"
  path_to_posts: System.fetch_env!("PATH_TO_POSTS")
