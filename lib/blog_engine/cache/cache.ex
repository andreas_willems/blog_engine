defmodule BlogEngine.Cache do
  use Agent

  def start_link(_args) do
    Agent.start_link(fn -> BlogEngine.Posts.Reader.get_posts() end, name: __MODULE__)
  end

  def get_posts() do
    Agent.get(__MODULE__, & &1)
  end
end
