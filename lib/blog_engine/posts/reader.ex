defmodule BlogEngine.Posts.Reader do
  alias BlogEngine.Posts.Parser

  defp posts_paths do
    Application.fetch_env!(:blog_engine, :path_to_posts)
    |> Path.wildcard()
    |> Enum.sort()
  end

  defp posts(posts_paths) do
    for post_path <- posts_paths do
      Parser.parse!(post_path)
    end
  end

  def get_posts do
    posts_paths()
    |> posts()
    |> Enum.sort_by(& &1.date, {:desc, Date})
  end
end
