defmodule BlogEngine do
  @moduledoc false

  defdelegate get_posts, to: BlogEngine.Cache
end
