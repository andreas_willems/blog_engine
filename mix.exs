defmodule BlogEngine.MixProject do
  use Mix.Project

  def project do
    [
      app: :blog_engine,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger, :earmark, :makeup_elixir],
      mod: {BlogEngine.Application, []}
    ]
  end

  defp deps do
    [
      {:earmark, "1.4.3"},
      {:makeup_elixir, "0.14.0"}
    ]
  end
end
